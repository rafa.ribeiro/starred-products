#!/usr/bin/env bash

echo "Applygin database migrations.."
python manage.py migrate

# Executar manualmente
#echo "Adding default super user..."
#python manage.py createsuperuser --email admin@starred-products.com --username admin

echo "Starting Starred-Products server..."
python manage.py runserver 0.0.0.0:8000