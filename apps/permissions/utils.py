#!/usr/bin/python3

from rest_framework.permissions import IsAuthenticatedOrReadOnly
from rest_framework.response import Response
from rest_framework.status import HTTP_401_UNAUTHORIZED


def has_permission(action: str, app_name: str, model_name: str):
    def decorator(decorated_function):

        def wrapper(self, *args, **kwargs):
            request = args[0]
            user = request.user

            permission_key = _mount_permission_key(action=action, app_name=app_name, model_name=model_name)

            if user.has_perm(permission_key) or _has_read_only_permissions(self):
                return decorated_function(self, *args, **kwargs)
            else:
                response_data = {
                    'message': "User doesn't have the authorization to execute this action",
                    'user': str(user),
                    'action': action}
                return Response(data=response_data, status=HTTP_401_UNAUTHORIZED)

        return wrapper

    return decorator


def _mount_permission_key(action: str, app_name: str, model_name: str) -> str:
    return f"{app_name.lower()}.{action.lower()}_{model_name.lower()}"


def _has_read_only_permissions(api_view_class):
    return IsAuthenticatedOrReadOnly in api_view_class.permission_classes
