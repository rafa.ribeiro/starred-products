#!/usr/bin/python3

from apps.product.models import Product


def get_product(product_id) -> Product:
    return Product.objects.get(product_id=product_id)
