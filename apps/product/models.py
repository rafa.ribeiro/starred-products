from django.db import models


class Product(models.Model):
    product_id = models.CharField(max_length=60, unique=True)

    class Meta:
        verbose_name = "Produto"
        verbose_name_plural = "Produtos"

    def __repr__(self):
        return f"{self.__class__.__name__}(product_id={self.product_id})"

    def __str__(self):
        return f"Product id: {self.product_id}"