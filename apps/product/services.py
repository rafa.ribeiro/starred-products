#!/usr/bin/python3
from django.core.exceptions import ObjectDoesNotExist

from apps.product import selectors
from apps.product.models import Product


def get_or_create_product(product_id: str) -> Product:
    try:
        product = selectors.get_product(product_id=product_id)
    except ObjectDoesNotExist:
        product = create_product(product_id=product_id)

    return product


def create_product(product_id: str) -> Product:
    product = Product(product_id=product_id)
    product.save()
    return product
