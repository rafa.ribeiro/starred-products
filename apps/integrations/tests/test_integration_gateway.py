#!/usr/bin/python3
import json

from django.test import TestCase

from apps.integrations.gateway import LuizaLabsGateway


class LuizaLabsIntegrationGatewayTestCase(TestCase):
    def test_get_product_integration(self):
        product_id = '00448f16-ff3d-30e8-a1c6-cf695eb6f0c9'

        product_response = LuizaLabsGateway().get_product_detail(product_id=product_id)

        self.assertEqual(product_response.status_code, 200)
        product_detail_data = json.loads(product_response.content.decode())

        self.assertIn('id', product_detail_data)
        self.assertIn('title', product_detail_data)
        self.assertIn('price', product_detail_data)
        self.assertIn('image', product_detail_data)
