#!/usr/bin/python3
import requests


class LuizaLabsGateway:
    base_product_url = "http://challenge-api.luizalabs.com/api/product/"

    def get_product_detail(self, product_id: str):
        product_detail_url = f"{self.base_product_url}{product_id}/"

        response = requests.get(url=product_detail_url)
        return response
