#!/usr/bin/python3
from django.db import models


class Customer(models.Model):
    name = models.CharField(max_length=200)
    email = models.EmailField(verbose_name='E-mail', unique=True)

    class Meta:
        verbose_name = "Cliente"
        verbose_name_plural = "Clientes"

    def __repr__(self):
        return f"{self.__class__.__name__}(name={self.name}, email={self.email})"

    def __str__(self):
        return f"Name: {self.name}, email={self.email}"
