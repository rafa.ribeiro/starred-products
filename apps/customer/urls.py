#!/usr/bin/python3

from django.conf.urls import include
from django.urls import path

from apps.customer.views import CustomerListApi, CustomerCreateApi, CustomerDetailApi, CustomerRemoveApi, \
    CustomerUpdateApi

customer_patterns = [
    path('', CustomerListApi.as_view(), name='list'),
    path('create/', CustomerCreateApi.as_view(), name='create'),
    path('<int:customer_id>/delete/', CustomerRemoveApi.as_view(), name='delete'),
    path('<int:customer_id>/update/', CustomerUpdateApi.as_view(), name='delete'),
    path('<int:customer_id>/', CustomerDetailApi.as_view(), name='detail'),
]

urlpatterns = [
    path('api/v1/customers/', include(customer_patterns)),
]
