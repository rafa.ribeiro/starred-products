#!/usr/bin/python3
from django.core.exceptions import ObjectDoesNotExist
from django.db.utils import IntegrityError

from apps.customer import selectors
from apps.customer.exceptions import CustomerNotFoundException, DuplicatedEmailCustomerException
from apps.customer.models import Customer


def get_customers():
    customers = selectors.all_customers()
    return customers


def create_customer(name: str, email: str) -> Customer:
    customer = Customer(name=name, email=email)

    try:
        customer.save()
    except IntegrityError as exc:
        raise DuplicatedEmailCustomerException(detail_error=str(exc))
    else:
        return customer


def get_customer(customer_id: int) -> Customer:
    try:
        customer = selectors.get_customer(customer_id=customer_id)
    except ObjectDoesNotExist:
        raise CustomerNotFoundException(customer_id=customer_id)
    else:
        return customer


def delete_customer(customer_id: int) -> None:
    customer = get_customer(customer_id=customer_id)
    customer.delete()


def update_customer(customer_id: int, name: str = None, email: str = None) -> Customer:
    customer = get_customer(customer_id=customer_id)
    customer.name = name or customer.name
    customer.email = email or customer.email

    try:
        customer.save()
    except IntegrityError as exc:
        raise DuplicatedEmailCustomerException(detail_error=str(exc))
    else:
        return customer
