#!/usr/bin/python3
from apps.customer.models import Customer


def create_and_save_customer(name: str, email: str) -> Customer:
    customer = create_customer(name=name, email=email)
    customer.save()
    return customer


def create_customer(name: str, email: str) -> Customer:
    return Customer(name=name, email=email)
