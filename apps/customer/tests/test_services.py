#!/usr/bin/python3
from django.test import TestCase

from apps.customer.exceptions import DuplicatedEmailCustomerException, CustomerNotFoundException
from apps.customer.models import Customer
from apps.customer.services import create_customer, delete_customer, update_customer, get_customer
from .fakedata import create_and_save_customer


class CustomerServicesTestCase(TestCase):
    def setUp(self):
        self.customer = create_and_save_customer(name='Johnny Lawrence', email='johnny.lawrence@cobrakai.com')

    def test_create_customer_works(self):
        self.assertEqual(Customer.objects.count(), 1)

        create_customer(name='Daniel Larusso', email='daniel.larusso@myagidokarate.com')

        self.assertEqual(Customer.objects.count(), 2)

    def test_create_customer_with_email_already_in_use(self):
        with self.assertRaises(DuplicatedEmailCustomerException) as context_manager:
            create_customer(name='John Kreese', email='johnny.lawrence@cobrakai.com')

        exception_content = context_manager.exception.args[0]
        self.assertIn('message', exception_content)
        self.assertIn('detail', exception_content)

    def test_delete_customer_works(self):
        other_customer = create_and_save_customer(name='Kesuke Miyagi', email='srmyagi@myagidokarate.com')
        self.assertEqual(Customer.objects.count(), 2)

        delete_customer(customer_id=other_customer.id)

        self.assertEqual(Customer.objects.count(), 1)

    def test_delete_customer_with_invalid_customer(self):
        with self.assertRaises(CustomerNotFoundException) as context_manager:
            delete_customer(customer_id=99)

        expected_message = f"Cliente de id = {99}, não encontrado."
        self.assertEqual(expected_message, str(context_manager.exception))

    def test_update_customer_works(self):
        old_name = self.customer.name
        updated_customer = update_customer(customer_id=self.customer.id, name='João Lourenço',
                                           email=self.customer.email)
        self.assertNotEqual(updated_customer.name, old_name)

    def test_update_customer_with_email_already_in_use(self):
        other_customer = create_and_save_customer(name='Robby Lawrence', email='robby@myagidokarate.com')

        with self.assertRaises(DuplicatedEmailCustomerException):
            update_customer(customer_id=other_customer.id, name=other_customer.name, email=self.customer.email)

    def test_get_customer_works(self):
        that_customer = get_customer(customer_id=self.customer.id)

        self.assertEqual(that_customer, self.customer)

    def test_get_customer_with_invalid_customer(self):
        with self.assertRaises(CustomerNotFoundException):
            get_customer(customer_id=99)
