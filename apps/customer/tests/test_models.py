#!/usr/bin/python3
from django.db.utils import IntegrityError
from django.test import TestCase

from .fakedata import create_and_save_customer, create_customer


class CustomerTestCase(TestCase):
    def setUp(self):
        create_and_save_customer(name='Thanos 2018', email='thanos@marvel.com')

    def test_no_duplicated_customer_email(self):
        other_customer = create_customer(name='Thanos 2014', email='thanos@marvel.com')
        with self.assertRaises(IntegrityError) as context_manager:
            other_customer.save()

        self.assertIn('duplicate key value violates unique constraint', str(context_manager.exception))
