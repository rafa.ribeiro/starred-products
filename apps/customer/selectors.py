#!/usr/bin/python3
from typing import Iterable

from apps.customer.models import Customer


def all_customers() -> Iterable[Customer]:
    return Customer.objects.all()


def get_customer(customer_id) -> Customer:
    return Customer.objects.get(id=customer_id)
