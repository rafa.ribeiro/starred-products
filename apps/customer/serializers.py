#!/usr/bin/python3
from rest_framework import serializers
from rest_framework.serializers import ModelSerializer, Serializer

from .models import Customer


class OutputSerializer(ModelSerializer):
    class Meta:
        model = Customer
        fields = ('id', 'name', 'email')


class InputSerializer(Serializer):
    name = serializers.CharField()
    email = serializers.EmailField()


class EditInputSerializer(Serializer):
    name = serializers.CharField(required=False)
    email = serializers.EmailField(required=False)
