#!/usr/bin/python3


class CustomerNotFoundException(Exception):
    def __init__(self, customer_id):
        super().__init__(f"Cliente de id = {customer_id}, não encontrado.")


class DuplicatedEmailCustomerException(Exception):
    def __init__(self, detail_error):
        error = {
            'message': "E-mail already in use by another customer.",
            'detail': detail_error,
        }
        super().__init__(error)
