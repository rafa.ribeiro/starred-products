#!/usr/bin/python3
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK, HTTP_201_CREATED, HTTP_409_CONFLICT, HTTP_400_BAD_REQUEST
from rest_framework.views import APIView

from apps.customer.exceptions import CustomerNotFoundException, DuplicatedEmailCustomerException
from apps.customer.serializers import OutputSerializer, InputSerializer, EditInputSerializer
from apps.customer.services import get_customers, create_customer, get_customer, delete_customer, update_customer
from apps.permissions.utils import has_permission


class CustomerListApi(APIView):
    permission_classes = (IsAuthenticated,)

    @has_permission(action='view', app_name='customer', model_name='Customer')
    def get(self, request):
        customers = get_customers()
        serializer = OutputSerializer(customers, many=True)
        return Response(data=serializer.data, status=HTTP_200_OK)


class CustomerCreateApi(APIView):
    permission_classes = (IsAuthenticated,)

    @has_permission(action='add', app_name='customer', model_name='Customer')
    def post(self, request):
        request_data = request.data
        serializer = InputSerializer(data=request_data)
        serializer.is_valid(raise_exception=True)

        try:
            create_customer(**serializer.validated_data)
        except DuplicatedEmailCustomerException as exc:
            return Response(data=str(exc), status=HTTP_409_CONFLICT)
        except Exception as exc:
            return Response(data=str(exc), status=HTTP_400_BAD_REQUEST)
        else:
            return Response(status=HTTP_201_CREATED)


class CustomerDetailApi(APIView):
    permission_classes = (IsAuthenticated,)

    @has_permission(action='view', app_name='customer', model_name='Customer')
    def get(self, request, customer_id):
        customer = get_customer(customer_id=customer_id)

        serializer = OutputSerializer(customer)
        return Response(data=serializer.data, status=HTTP_200_OK)


class CustomerRemoveApi(APIView):
    permission_classes = (IsAuthenticated,)

    @has_permission(action='delete', app_name='customer', model_name='Customer')
    def delete(self, request, customer_id):
        try:
            delete_customer(customer_id=customer_id)
        except CustomerNotFoundException as exc:
            return Response(data=str(exc), status=HTTP_400_BAD_REQUEST)
        else:
            return Response(status=HTTP_200_OK)


class CustomerUpdateApi(APIView):
    permission_classes = (IsAuthenticated,)

    @has_permission(action='change', app_name='customer', model_name='Customer')
    def put(self, request, customer_id):
        serializer = EditInputSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        try:
            updated_customer = update_customer(customer_id=customer_id, **serializer.validated_data)
        except DuplicatedEmailCustomerException as exc:
            return Response(data=str(exc), status=HTTP_409_CONFLICT)
        except Exception as exc:
            return Response(data=str(exc), status=HTTP_400_BAD_REQUEST)
        else:
            output_serializer = OutputSerializer(updated_customer)
            return Response(data=output_serializer.data, status=HTTP_200_OK)
