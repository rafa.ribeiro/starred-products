#!/usr/bin/python3

import json
from unittest import mock

from django.core.cache import cache
from django.test import TestCase
from django_fakeredis import FakeRedis
from rest_framework.status import HTTP_200_OK, HTTP_404_NOT_FOUND, HTTP_400_BAD_REQUEST

from apps.core.adapter import ProductDetail
from apps.core.exceptions import ProductNotFoundException, UnknownProductDetailException
from apps.core.models import FavoriteProduct
from apps.core.services import get_product_detail, create_favorite_product_item
from .fakedata import create_and_save_customer
from .mocker import mock_product_detail, MockResponse


class FavoriteProductServicesTestCase(TestCase):
    @FakeRedis(path='django.core.cache.cache')
    def setUp(self):
        cache.clear()

    @FakeRedis(path='django.core.cache.cache')
    @mock.patch('apps.integrations.gateway.LuizaLabsGateway.get_product_detail')
    def test_get_product_detail_works(self, mocked_get_product_detail):
        mocked_product_detail = mock_product_detail(id='00448f16-ff3d-30e8-a1c6-cf695eb6f0c9',
                                                    title='Guitarra Strato Fender Amercian Special',
                                                    brand='fender',
                                                    image='http://challenge-api.luizalabs.com/images/00448f16-ff3d-30e8-a1c6-cf695eb6f0c9.jpg',
                                                    price=8849.9)

        mocked_content_json = json.dumps(mocked_product_detail).encode()
        mocked_get_product_detail.return_value = MockResponse(content=mocked_content_json, status_code=HTTP_200_OK)

        product_detail = get_product_detail(product_id=mocked_product_detail['id'])

        self.assertIsInstance(product_detail, ProductDetail)
        mocked_get_product_detail.assert_called_once_with(product_id=mocked_product_detail['id'])

        that_product_detail = get_product_detail(product_id=mocked_product_detail['id'])
        mocked_get_product_detail.assert_called_once()

        self.assertEqual(product_detail, that_product_detail)

    @mock.patch('apps.integrations.gateway.LuizaLabsGateway.get_product_detail')
    def test_get_product_detail_not_found(self, mocked_get_product_detail):
        mocked_get_product_detail.return_value = MockResponse(status_code=HTTP_404_NOT_FOUND)

        with self.assertRaises(ProductNotFoundException):
            get_product_detail(product_id='invalid_product_id')

    @mock.patch('apps.integrations.gateway.LuizaLabsGateway.get_product_detail')
    def test_get_product_detail_with_unknown_error(self, mocked_get_product_detail):
        mocked_get_product_detail.return_value = MockResponse(content='Wild unknown error appeared',
                                                              status_code=HTTP_400_BAD_REQUEST)

        with self.assertRaises(UnknownProductDetailException):
            get_product_detail(product_id='any_product_id')

    @FakeRedis(path='django.core.cache.cache')
    @mock.patch('apps.integrations.gateway.LuizaLabsGateway.get_product_detail')
    def test_create_favorite_product_item_works(self, mocked_get_product_detail):
        product_id = '00448f16-ff3d-30e8-a1c6-cf695eb6f0c9'

        mocked_product_detail = mock_product_detail(id=product_id,
                                                    title='Guitarra Strato Fender Amercian Special',
                                                    brand='fender',
                                                    image='http://challenge-api.luizalabs.com/images/00448f16-ff3d-30e8-a1c6-cf695eb6f0c9.jpg',
                                                    price=8849.9)

        mocked_content_json = json.dumps(mocked_product_detail).encode()
        mocked_get_product_detail.return_value = MockResponse(content=mocked_content_json, status_code=HTTP_200_OK)

        customer = create_and_save_customer(name='Ringo Starr', email='ringo.starr@beatles.com')

        favorite_product = create_favorite_product_item(customer_id=customer.id, product_id=product_id)

        self.assertEqual(favorite_product.customer_id, customer.id)
        self.assertEqual(favorite_product.product.product_id, product_id)
        self.assertEqual(FavoriteProduct.objects.count(), 1)
