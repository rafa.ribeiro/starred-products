#!/usr/bin/python3

from typing import Optional, Dict
from unittest import mock

from requests.models import Response


def mock_product_detail(id: str, title: str, brand: str, image: str, price: float,
                        review_score: Optional[float] = None) -> Dict:
    return {
        'id': id,
        'title': title,
        'brand': brand,
        'image': image,
        'price': price,
        'reviewScore': review_score
    }


class MockResponse(mock.Mock):
    def __init__(self, content=b'', status_code=None):
        super().__init__(spec=Response)
        self.status_code = status_code
        self.content = content
