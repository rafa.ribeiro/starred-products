#!/usr/bin/python3
from django.test import SimpleTestCase

from apps.core.adapter import adapt_products_detail, ProductDetail
from .mocker import mock_product_detail


class FavoriteProductAdapterTestCase(SimpleTestCase):
    def test_adapter_with_partial_product_info(self):
        mocked_product_detail = mock_product_detail(id='00448f16-ff3d-30e8-a1c6-cf695eb6f0c9',
                                                    title='Guitarra Strato Fender Amercian Special',
                                                    brand='fender',
                                                    image='http://challenge-api.luizalabs.com/images/00448f16-ff3d-30e8-a1c6-cf695eb6f0c9.jpg',
                                                    price=8849.9)

        adapted_product = adapt_products_detail(product_detail=mocked_product_detail)

        self.assertIsInstance(adapted_product, ProductDetail)
        self.assertEqual(adapted_product.id, mocked_product_detail['id'])
        self.assertEqual(adapted_product.title, mocked_product_detail['title'])
        self.assertEqual(adapted_product.image, mocked_product_detail['image'])
        self.assertEqual(adapted_product.price, mocked_product_detail['price'])
        self.assertEqual(adapted_product.review_score, None)

    def test_adapter_with_complete_product_info(self):
        mocked_product_detail = mock_product_detail(id='79b1c283-00ef-6b22-1c8d-b0721999e2f0',
                                                    title='Aparelho de Muscula\u00e7\u00e3o Academia Particular',
                                                    brand='polimet',
                                                    image='http://challenge-api.luizalabs.com/images/79b1c283-00ef-6b22-1c8d-b0721999e2f0.jpg',
                                                    price=799.9,
                                                    review_score=4.4166665)

        adapted_product = adapt_products_detail(product_detail=mocked_product_detail)

        self.assertIsInstance(adapted_product, ProductDetail)
        self.assertEqual(adapted_product.id, mocked_product_detail['id'])
        self.assertEqual(adapted_product.title, mocked_product_detail['title'])
        self.assertEqual(adapted_product.image, mocked_product_detail['image'])
        self.assertEqual(adapted_product.price, mocked_product_detail['price'])
        self.assertEqual(adapted_product.review_score, mocked_product_detail['reviewScore'])
