#!/usr/bin/python3


from django.test import TestCase

from apps.core.selectors import list_products_by_customer
from .fakedata import create_and_save_product, create_and_save_customer, create_and_save_favorite_product


class FavoriteProductSelectorsTestCase(TestCase):
    def setUp(self):
        self.customer_paul = create_and_save_customer(name='Paul', email='paul.mccartney@beatles.com')
        self.customer_george = create_and_save_customer(name='George', email='george.harrison@beatles.com')

        self.product_bass = create_and_save_product(product_id='bass_id')
        self.product_piano = create_and_save_product(product_id='piano_id')
        self.product_guitar = create_and_save_product(product_id='guitar_id')

    def test_list_favorites_products_by_client_works(self):
        create_and_save_favorite_product(customer_id=self.customer_paul.id, product_id=self.product_bass.id)
        create_and_save_favorite_product(customer_id=self.customer_paul.id, product_id=self.product_piano.id)

        create_and_save_favorite_product(customer_id=self.customer_george.id, product_id=self.product_guitar.id)

        pauls_favorites = list_products_by_customer(self.customer_paul.id)
        self.assertEqual(len(pauls_favorites), 2)

        georges_favorites = list_products_by_customer(self.customer_george.id)
        self.assertEqual(len(georges_favorites), 1)
