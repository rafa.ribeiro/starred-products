#!/usr/bin/python3
from django.db.utils import IntegrityError
from django.test import TestCase

from .fakedata import create_and_save_customer, create_and_save_product, create_favorite_product


class FavoriteProductModelTestCase(TestCase):
    def setUp(self):
        self.customer = create_and_save_customer(name='John', email='john.lennon@gmail.com')
        self.product = create_and_save_product(product_id='2b505fab-d865-e164-345d-efbd4c2045b6')

    def test_no_duplicated_products_on_favorites_list(self):
        favorite = create_favorite_product(customer_id=self.customer.id, product_id=self.product.id)
        favorite.save()

        other_favorite = create_favorite_product(customer_id=self.customer.id, product_id=self.product.id)
        with self.assertRaises(IntegrityError) as context_manager:
            other_favorite.save()

        self.assertIn('duplicate key value violates unique constraint', str(context_manager.exception))
