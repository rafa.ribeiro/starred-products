#!/usr/bin/python3


from apps.core.models import FavoriteProduct
from apps.customer.tests import fakedata
from apps.product.models import Product


def create_and_save_customer(name: str, email: str):
    return fakedata.create_and_save_customer(name=name, email=email)


def create_and_save_product(product_id: str) -> Product:
    product = Product(product_id=product_id)
    product.save()
    return product


def create_and_save_favorite_product(customer_id: int, product_id: str) -> FavoriteProduct:
    favorite = create_favorite_product(customer_id=customer_id, product_id=product_id)
    favorite.save()
    return favorite


def create_favorite_product(customer_id: int, product_id: str) -> FavoriteProduct:
    return FavoriteProduct(customer_id=customer_id, product_id=product_id)
