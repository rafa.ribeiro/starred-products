from django.contrib import admin

from apps.core.models import FavoriteProduct

admin.site.register(FavoriteProduct)
