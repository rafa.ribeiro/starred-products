#!/usr/bin/python3

from dataclasses import dataclass
from typing import Dict, Optional


@dataclass
class ProductDetail:
    id: str
    title: str
    image: str
    price: float
    review_score: Optional[float]


def adapt_products_detail(product_detail: Dict) -> ProductDetail:
    id = product_detail.get('id')
    title = product_detail.get('title')
    image = product_detail.get('image')
    price = product_detail.get('price')
    review_score = product_detail.get('reviewScore', None)

    return ProductDetail(id=id, title=title, image=image, price=price, review_score=review_score)
