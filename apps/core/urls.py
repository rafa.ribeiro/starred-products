#!/usr/bin/python3

from django.conf.urls import include
from django.urls import path

from apps.core.views import FavoriteProductCreateApi, FavoriteProductDetailListByCustomerApi, FavoriteProductDeleteApi

favorites_patterns = [
    path('create/', FavoriteProductCreateApi.as_view(), name='create'),
    path('<int:customer_id>/list-products/', FavoriteProductDetailListByCustomerApi.as_view(), name='list_products'),
    path('delete/', FavoriteProductDeleteApi.as_view(), name='delete'),
]

urlpatterns = [
    path('api/v1/favorites/', include(favorites_patterns)),
]
