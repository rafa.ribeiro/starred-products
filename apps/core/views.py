#!/usr/bin/python3
from rest_framework.permissions import IsAuthenticated, IsAuthenticatedOrReadOnly
from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK, HTTP_412_PRECONDITION_FAILED, HTTP_400_BAD_REQUEST
from rest_framework.views import APIView

from apps.core.serializers import InputSerializer, ProductDetailOutputSerializer
from apps.core.services import create_favorite_product_item, list_products_detail, remove_favorite_product
from apps.permissions.utils import has_permission


class FavoriteProductCreateApi(APIView):
    permission_classes = (IsAuthenticated,)

    @has_permission(action='add', app_name='core', model_name='FavoriteProduct')
    def post(self, request):
        serializer = InputSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        try:
            create_favorite_product_item(**serializer.validated_data)
        except Exception as exc:
            return Response(data=str(exc), status=HTTP_412_PRECONDITION_FAILED)
        else:
            return Response(status=HTTP_200_OK)


class FavoriteProductDetailListByCustomerApi(APIView):
    permission_classes = (IsAuthenticatedOrReadOnly,)

    @has_permission(action='view', app_name='core', model_name='FavoriteProduct')
    def get(self, request, customer_id):
        products = list_products_detail(customer_id=customer_id)

        serializer = ProductDetailOutputSerializer(products, many=True)
        return Response(data=serializer.data, status=HTTP_200_OK)


class FavoriteProductDeleteApi(APIView):
    permission_classes = (IsAuthenticated,)

    @has_permission(action='delete', app_name='core', model_name='FavoriteProduct')
    def delete(self, request):
        serializer = InputSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        try:
            remove_favorite_product(**serializer.validated_data)
        except Exception as exc:
            return Response(data=str(exc), status=HTTP_400_BAD_REQUEST)
        else:
            return Response(status=HTTP_200_OK)
