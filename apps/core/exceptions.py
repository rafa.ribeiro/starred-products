#!/usr/bin/python3

class UnknownProductDetailException(Exception):
    def __init__(self, status_code, error_message):
        super().__init__({'status': status_code, 'detail': error_message})


class ProductNotFoundException(Exception):
    def __init__(self, product_id):
        super().__init__(f"Produto de id = {product_id}, não encontrado.")


class DuplicatedProductOnFavoritesListException(Exception):
    def __init__(self, detail_error):
        error = {
            'message': "Product already in the customer favorites list.",
            'detail': detail_error,
        }
        super().__init__(error)


class FavoriteProductNotFoundException(Exception):
    def __init__(self, customer_id, product_id):
        message = f"Favorite product not found for: Customer = {customer_id}, Product: {product_id}"
        super().__init__(message)
