#!/usr/bin/python3
from rest_framework import serializers
from rest_framework.serializers import Serializer


class InputSerializer(Serializer):
    customer_id = serializers.IntegerField()
    product_id = serializers.CharField()


class ProductDetailOutputSerializer(Serializer):
    id = serializers.CharField()
    title = serializers.CharField()
    image = serializers.CharField()
    price = serializers.FloatField()
    review_score = serializers.FloatField()
