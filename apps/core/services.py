#!/usr/bin/python3
import json
from typing import List

from django.core.cache import cache
from django.core.exceptions import ObjectDoesNotExist
from django.db.utils import IntegrityError

from apps.core.adapter import adapt_products_detail, ProductDetail
from apps.core.exceptions import UnknownProductDetailException, ProductNotFoundException, \
    DuplicatedProductOnFavoritesListException, FavoriteProductNotFoundException
from apps.core.models import FavoriteProduct
from apps.core.selectors import list_products_by_customer, get_favorite_product
from apps.customer.services import get_customer
from apps.integrations.gateway import LuizaLabsGateway
from apps.product.services import get_or_create_product

STATUS_200_OK = 200
STATUS_404_NOT_FOUND = 404
ONE_MINUTE = 60
ONE_HOUR = 60 * ONE_MINUTE
ONE_DAY = ONE_HOUR * 24


def create_favorite_product_item(customer_id: int, product_id: str) -> FavoriteProduct:
    try:
        _is_valid_product(product_id=product_id)
    except Exception as exc:
        raise exc

    customer = get_customer(customer_id=customer_id)
    product = get_or_create_product(product_id=product_id)

    favorite = FavoriteProduct(customer=customer, product=product)
    try:
        favorite.save()
    except IntegrityError as exc:
        raise DuplicatedProductOnFavoritesListException(detail_error=str(exc))
    else:
        return favorite


def _is_valid_product(product_id: str):
    try:
        get_product_detail(product_id=product_id)
    except (ProductNotFoundException, UnknownProductDetailException) as exc:
        raise exc


def list_products_detail(customer_id: int) -> List[ProductDetail]:
    favorites = list_products_by_customer(customer_id=customer_id)

    products = list()

    for favorite in favorites:
        product_id = favorite.product.product_id
        product = get_product_detail(product_id=product_id)
        products.append(product)

    return products


def get_product_detail(product_id):
    cache_key = f"product-id_{product_id}"

    cached_result = cache.get(cache_key)
    if cached_result is None:
        response = LuizaLabsGateway().get_product_detail(product_id=product_id)

        if response.status_code == STATUS_200_OK:
            content = json.loads(response.content.decode())
            product_detail = adapt_products_detail(product_detail=content)

            cache.set(cache_key, product_detail, ONE_DAY * 2)

        elif response.status_code == STATUS_404_NOT_FOUND:
            raise ProductNotFoundException(product_id=product_id)
        else:
            raise UnknownProductDetailException(status_code=response.status_code, error_message=response.content)
    else:
        product_detail = cached_result

    return product_detail


def remove_favorite_product(customer_id: int, product_id: str):
    try:
        favorite_product = get_favorite_product(customer_id=customer_id, product_id=product_id)
    except ObjectDoesNotExist:
        raise FavoriteProductNotFoundException(customer_id=customer_id, product_id=product_id)
    else:
        favorite_product.delete()
