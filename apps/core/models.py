from django.db import models

from apps.customer.models import Customer
from apps.product.models import Product

class FavoriteProduct(models.Model):
    customer = models.ForeignKey(Customer, verbose_name='Cliente', on_delete=models.CASCADE)
    product = models.ForeignKey(Product, verbose_name='Produto favorito', on_delete=models.CASCADE)

    class Meta:
        verbose_name = "Produto favorito de cliente"
        unique_together = ('customer', 'product')

    def __repr__(self):
        return f"{self.__class__.__name__}(customer={self.customer_id}, product={self.product_id})"

    def __str__(self):
        return f"Customer: {self.customer_id} -> Product: {self.product_id}"