#!/usr/bin/python3

from typing import List

from apps.core.models import FavoriteProduct


def list_products_by_customer(customer_id: int) -> List[FavoriteProduct]:
    return FavoriteProduct.objects.filter(customer_id=customer_id).select_related('product')


def get_favorite_product(customer_id: int, product_id: str) -> FavoriteProduct:
    return FavoriteProduct.objects.get(customer_id=customer_id, product__product_id=product_id)
