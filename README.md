# Starred Products

Starred Products é o projeto de um backend que tem como missão gerenciar a lista de produtos favoritos dos clientes Magalu.

Esse backend irá prover através de APIs no padrão REST as funcionalidades necessárias para todo o gerenciamento de clientes bem
como de sua lista de produtos favoritos.

Esse projeto será utilizado principalmente para campanhas de marketing visando oferecer produtos mais relevantes para o cliente.


## O projeto

O projeto foi desenvolvido utilizando-se Python e do framework Django. Para facilitar o processo de entrega e um possível
deploy da aplicação, o projeto pode ser executado utilizando-se de contêiners Docker.

 
## Requisitos para instalação
 
Para instalação e uso do projeto é necessário ter instalado em sua máquina o docker e o docker-compose. 
Durante o desenvolvimento da aplicação foram usadas as versões abaixo:
 
- **Docker:** Docker version 19.03.8, build afacb8b
- **Docker Compose:** docker-compose version 1.25.4, build 8d51620a


## Instalação

O código do projeto encontra-se hospedado no GitLab e pode ser acessado em:
 
 [https://gitlab.com/rafa.ribeiro/starred-products]

Clone o projeto para sua máquina (necessário o Git) ou faça o download do repositório.
 
 
Feito o clone, via terminal, acesse o diretório do projeto:

```
cd starred-products
```

Dentro do diretório _starred-products_, execute o comando:

```
docker-compose up
```

Ao executar o comando, o arquivo _docker-compose.yml_ será lido e a partir dele serão construídos os contêiners necessários 
para a aplicação.

Após sua execução a janela do terminal ficará bloqueada sinalizando que a aplicação está rodando, 
deverá ser mostrado algo como:

```
web_1          | System check identified no issues (0 silenced).
web_1          | September 25, 2020 - 04:47:40
web_1          | Django version 3.0, using settings 'config.settings'
web_1          | Starting development server at http://0.0.0.0:8000/
web_1          | Quit the server with CONTROL-C.
```

Execute o comando **docker ps** para verificar que os contêiners docker estão em execução corretamente:

```
docker ps
    

CONTAINER ID        IMAGE                  COMMAND                  CREATED             STATUS              PORTS                    NAMES
4d2d10b5692f        starred-products_web   "./wait-for-it.sh db…"   10 minutes ago      Up 10 minutes       0.0.0.0:8000->8000/tcp   starred-products_web_1
401539100d80        redis                  "docker-entrypoint.s…"   10 minutes ago      Up 10 minutes       0.0.0.0:6379->6379/tcp   redis-cache
ed5ce9805dbc        postgres               "docker-entrypoint.s…"   10 minutes ago      Up 10 minutes       0.0.0.0:5432->5432/tcp   starred-products_db_1
```

A aplicação lida com dados de clientes que temos obrigação de proteger, por isso antes de continuar com o uso,
é necessário fazer algumas parametrizações.

### Criando um superuser no Django-Admin:

Use o comando abaixo para criar um superuser no Django-Admin. Esse usuário admin será responsável por poder criar outros
usuários para a aplicação e por fornecer-lhes permissão a diferentes ações:

```
docker-compose run web python manage.py createsuperuser
```

Ao executar, será lhe pedido que forneça um username para o superuser e uma senha de acesso, algo como mostado abaixo:

```
Username (leave blank to use 'root'): <INSERIR_USERNAME>
Email address: <INSERIR_EMAIL> // Opcional
Password: <INSERIR_SENHA>
Password (again): <REPETIR_SENHA>
Superuser created successfully.
```

Com o usuário criado, acesse o endereço abaixo e forneça suas credenciais de autenticação:

[http://localhost:8000/admin/]


Ao acessar o painel de administração do Django será possível observar os modelos disponíveis na aplicação.

Na seção **AUTHENTICATION AND AUTHORIZATION**, clique em **Users** e após clique em **ADD USER +**

Crie, pelo menos, um novo usuário para utilizarmos como usuário operador da aplicação, com ele poderemos testar
as funcionalidades de permissões que a aplicação contém.

Insira um nome e senha para o novo usuário e clique em **Salvar**.

Na sequência serão solicitadas mais informações não obrigatórias, mas o interessante aqui é nos atentarmos
para a seção **Permissions**, nela há a opção **User permissions** e é aqui que iremos selecionar todas as ações
que o nosso novo usuário terá permissão para executar.

Há várias opções, mas para o nosso projeto precisaremos principalmente das permissões referentes aos seguintes modelos:

- customer  | Cliente
- core      | Produto favorito de cliente


Para cada modelo há as seguintes permissões que indicam uma ação possível de ser executada pelo usuário:

- Add: Permissão para criar um novo registro
- Change: Permissão para atualizar um registro
- Delete: Permissão para remover um registro existente
- View: Permissão para visualizar um registro


Selecione as permissões desejadas e clique na seta para a direita para adicioná-las ao nosso novo usuário.


Feito isso, temos um usuário operador e agora podemos começar a utilizar de fato a aplicação.


## API Starred-Products

No link abaixo, há um documento listando todos os endpoints disponíveis na aplicação com exemplos de uso
dos endpoints, evidenciando os parâmetros e as respostas de cada endpoint.

[https://documenter.getpostman.com/view/11425033/TVKG1GNi]


## Executando os testes

Para execução dos testes, é possível executar o comando abaixo:

```
docker-compose run web python manage.py test
```

Ao executar com esse comando, os testes serão executados dentro do contêiner docker.
 
É possível rodar os testes fora do contêiner também, ou seja, na máquina local, mas para isso é preciso ter o **pipenv** 
devidamente instalado e estar com o ambiente virtual ativo. Para ativá-lo basta estar no diretório raiz do projeto e 
executar o comando:

```
pipenv shell
```

Com o ambiente virtual ativado, é preciso executar o comando abaixo para instalar todas as dependências do projeto 
nesse ambiente:

```
pipenv install
```

Uma vez que as dependências foram instaladas, executar o comando abaixo para carregar variáveis de ambiente que 
devem ser utilizadas para quando executarmos a aplicação fora do contêiner:

```
source export_local_test_vars.sh
```

Feito isso, é possível rodar os testes localmente utilizando o comando:

```
python manage.py test
```

## ToDos / Desejos / Melhorias para o projeto

- Adicionar algum agendador (cron) de tarefas para executar tasks periódicas em background para renovar o cache de produtos.
- Acrescentar arquivos *.env para armazenar dados sensíveis como, por exemplo, usuário de conexão com o banco de dados.
- Adicionar scripts para encriptar/desencriptar conteúdo de arquivos envs e commitar somente os encriptados para melhorar
 segurança dos dados sensíveis da aplicação.
- Criar hook para executar os testes de unidade a cada commit ou push feito.
- O projeto tem um teste de integração que acessa um conteúdo externo de fato, seria ideal isolar esse teste para não ser
executado de forma padrão pelo python manage.py test e executar em momentos específicos.